package ua.danit;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.FlushModeType;
import javax.persistence.LockModeType;
import javax.persistence.Query;

import ua.danit.application.Ticket;


public class DanItEntityManager implements EntityManager {
  public DanItEntityManager(Class<Ticket> ticketClass) {
  }

  @Override
  public void persist(Object o) {

  }

  @Override
  public <T> T merge(T t) {
    return null;
  }

  @Override
  public void remove(Object o) {

  }

  @Override
  public <T> T find(Class<T> someClass, Object o) {
    return null;
  }

  @Override
  public <T> T getReference(Class<T> someClass, Object o) {
    return null;
  }

  @Override
  public void flush() {

  }

  @Override
  public FlushModeType getFlushMode() {
    return null;
  }

  @Override
  public void setFlushMode(FlushModeType flushModeType) {

  }

  @Override
  public void lock(Object o, LockModeType lockModeType) {

  }

  @Override
  public void refresh(Object o) {

  }

  @Override
  public void clear() {

  }

  @Override
  public boolean contains(Object o) {
    return false;
  }

  @Override
  public Query createQuery(String s) {
    return null;
  }

  @Override
  public Query createNamedQuery(String s) {
    return null;
  }

  @Override
  public Query createNativeQuery(String s) {
    return null;
  }

  @Override
  public Query createNativeQuery(String s, Class someClass) {
    return null;
  }

  @Override
  public Query createNativeQuery(String s, String s1) {
    return null;
  }

  @Override
  public void joinTransaction() {

  }

  @Override
  public Object getDelegate() {
    return null;
  }

  @Override
  public void close() {

  }

  @Override
  public boolean isOpen() {
    return false;
  }

  @Override
  public EntityTransaction getTransaction() {
    return null;
  }
}
